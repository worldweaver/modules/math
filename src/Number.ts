import IMathObject from "./IMathObject";
import IReadOnlyNumber from "./IReadOnlyNumber";

export default class Number implements IReadOnlyNumber, IMathObject<Number>
{
	private _value: number;
	get value() : number
	{
		return this._value;
	}
	set value(value: number)
	{
		this._value = value;
	}
	setValue(index: number, value: number)
	{
		if(index != 0)
		{
			throw new Error("Number only has 1 value. Attempted to set value index " + index);
		}
		this.value = value;
	}

	get values(): Array<number>
	{
		return [this.value];
	}
	set values(values: Array<number>)
	{
		this.value = values[0];
	}

	constructor(value: number = 0)
	{
		this._value = value;
	}

	add(value: Number): Number
	{
		return new Number(this.value + value.value);
	}

	addToThis(value: Number): void
	{
		this.value += value.value;
	}

	divide(value: Number): Number
	{
		return new Number(this.value / value.value);
	}

	divideScalar(value: number): Number
	{
		return new Number(this.value / value);
	}

	divideThis(value: Number): void
	{
		this.value /= value.value;
	}

	divideThisScalar(value: number): void
	{
		this.value /= value;
	}

	multiply(value: Number): Number
	{
		return new Number(this.value * value.value);
	}

	multiplyScalar(value: number): Number
	{
		return new Number(this.value * value);
	}

	multiplyThis(value: Number): void
	{
		this.value *= value.value;
	}

	multiplyThisScalar(value: number): void
	{
		this.value *= value;
	}

	subtract(value: Number): Number
	{
		return new Number(this.value - value.value);
	}

	subtractFromThis(value: Number): void
	{
		this.value -= value.value
	}

	clone(): Number
	{
		return new Number(this.value);
	}

	equals(other: Number): Boolean
	{
		return this.value == other.value;
	}
}