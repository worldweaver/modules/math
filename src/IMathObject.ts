export default interface IMathObject<T>
{
	get values(): Array<number>;
	set values(values: Array<number>);

	setValue(index: number, value: number): void;

	clone(): T;
	equals(other: T): Boolean;

	add(value: T) : T;
	addToThis(value: T): void;
	subtract(value: T) : T;
	subtractFromThis(value: T) : void;
	multiply(value: T) : T;
	multiplyThis(value: T) : void;
	multiplyScalar(value: number) : T;
	multiplyThisScalar(value: number) : void;
	divide(value: T) : T;
	divideThis(value: T) : void;
	divideScalar(value: number) : T;
	divideThisScalar(value: number) : void;
 }