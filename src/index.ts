import Color from "./Color";
import IMathObject from "./IMathObject";
import IReadOnlyColor from "./IReadOnlyColor";
import IReadOnlyNumber from "./IReadOnlyNumber";
import IReadOnlyRange from "./IReadOnlyRange";
import IReadOnlyVector2 from "./IReadOnlyVector2";
import Number from "./Number";
import Range from "./Range";
import Vector2 from "./Vector2";

export {
	Color,
	IMathObject,
	IReadOnlyColor,
	IReadOnlyNumber,
	IReadOnlyRange,
	IReadOnlyVector2,
	Number,
	Range,
	Vector2
}