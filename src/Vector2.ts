import IMathObject from "./IMathObject";
import {Point} from "pixi.js";
import IReadOnlyVector2 from "./IReadOnlyVector2";

export default class Vector2 implements IReadOnlyVector2, IMathObject<Vector2>
{
	private readonly _point: Point;

	get x() : number
	{
		return this._point.x;
	}
	get y() : number
	{
		return this._point.y;
	}

	set x(x : number)
	{
		this._point.x = x;
	}
	set y(y : number)
	{
		this._point.y = y;
	}

	get values(): Array<number>
	{
		return [this.x, this.y];
	}
	set values(values: Array<number>)
	{
		this.x = values[0];
		this.y = values[1];
	}
	setValue(index: number, value: number)
	{
		switch (index)
		{
			case 0: this.x = value; break;
			case 1: this.y = value; break;
			default: throw new Error("Vector2 only has 2 values. Attempted to set value index " + index);
		}
	}

	get asPoint() : Point
	{
		return this._point;
	}

	constructor(x: number = 0, y: number = 0)
	{
		this._point = new Point(x, y);
	}

	add(value: Vector2): Vector2
	{
		return new Vector2(this.x + value.x, this.y + value.y);
	}

	addToThis(value: Vector2): void
	{
		this.x += value.x;
		this.y += value.y;
	}

	divide(value: Vector2): Vector2
	{
		return new Vector2(this.x / value.x, this.y / value.y);
	}

	divideScalar(value: number): Vector2
	{
		return new Vector2(this.x / value, this.y / value);
	}

	divideThis(value: Vector2): void
	{
		this.x /= value.x;
		this.y /= value.y;
	}

	divideThisScalar(value: number): void
	{
		this.x /= value;
		this.y /= value;
	}

	multiply(value: Vector2): Vector2
	{
		return new Vector2(this.x * value.x, this.y * value.y);
	}

	multiplyScalar(value: number): Vector2
	{
		return new Vector2(this.x * value, this.y * value);
	}

	multiplyThis(value: Vector2): void
	{
		this.x *= value.x;
		this.y *= value.y;
	}

	multiplyThisScalar(value: number): void
	{
		this.x *= value;
		this.y *= value;
	}

	subtract(value: Vector2): Vector2
	{
		return new Vector2(this.x - value.x, this.y - value.y);
	}

	subtractFromThis(value: Vector2): void
	{
		this.x -= value.x;
		this.y -= value.y;
	}

	clone(): Vector2
	{
		return new Vector2(this.x, this.y);
	}

	equals(other: Vector2): Boolean
	{
		return this.x == other.x && this.y == other.y;
	}
}