import IMathObject from "./IMathObject";

export default interface IReadOnlyRange<T extends IMathObject<T>>
{
	get min(): T;
	get max(): T;
}