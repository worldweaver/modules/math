export default interface IReadOnlyVector2
{
	get x(): number;
	get y(): number;
}