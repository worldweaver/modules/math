export default interface IReadOnlyColor
{
	get r(): number;
	get g(): number;
	get b(): number;
	get a(): number;
}