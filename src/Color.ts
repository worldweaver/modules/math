import IMathObject from "./IMathObject";
import IReadOnlyColor from "./IReadOnlyColor";

export default class Color implements IReadOnlyColor, IMathObject<Color>
{
	private _r: number;
	private _g: number;
	private _b: number;
	private _a: number;

	get r(): number
	{
		return this._r;
	}
	set r(value: number)
	{
		this._r = value;
	}

	get g(): number
	{
		return this._g;
	}
	set g(value: number)
	{
		this._g = value;
	}

	get b(): number
	{
		return this._b;
	}
	set b(value: number)
	{
		this._b = value;
	}

	get a(): number
	{
		return this._a;
	}
	set a(value: number)
	{
		this._a = value;
	}

	get values(): Array<number>
	{
		return [this.r, this.g, this.b, this.a];
	}
	set values(values: Array<number>)
	{
		this.r = values[0];
		this.g = values[1];
		this.b = values[2];
		this.a = values[3];
	}

	setValue(index: number, value: number): void
	{
		switch (index)
		{
			case 0: this.r = value; break;
			case 1: this.g = value; break;
			case 2: this.b = value; break;
			case 3: this.a = value; break;
			default: throw new Error("Color only has 4 values. Attempted to set index " + index);
		}
	}

	constructor(r: number = 1, g: number = 1, b: number = 1, a: number = 1)
	{
		this._r = r;
		this._g = g;
		this._b = b;
		this._a = a;
	}

	add(value: Color): Color
	{
		return new Color(
			this._r + value._r,
			this._g + value._g,
			this._b + value._b,
			this._a + value._a
		);
	}

	addToThis(value: Color): void
	{
		this._r += value._r;
		this._g += value._g;
		this._b += value._b;
		this._a += value._a;
	}

	divide(value: Color): Color
	{
		return new Color(
			this._r / value._r,
			this._g / value._g,
			this._b / value._b,
			this._a / value._a
		);
	}

	divideScalar(value: number): Color
	{
		return new Color(
			this._r / value,
			this._g / value,
			this._b / value,
			this._a / value
		);
	}

	divideThis(value: Color): void
	{
		this._r /= value._r;
		this._g /= value._g;
		this._b /= value._b;
		this._a /= value._a;
	}

	divideThisScalar(value: number): void
	{
		this._r /= value;
		this._g /= value;
		this._b /= value;
		this._a /= value;
	}

	multiply(value: Color): Color
	{
		return new Color(
			this._r * value._r,
			this._g * value._g,
			this._b * value._b,
			this._a * value._a
		);
	}

	multiplyScalar(value: number): Color
	{
		return new Color(
			this._r * value,
			this._g * value,
			this._b * value,
			this._a * value
		);
	}

	multiplyThis(value: Color): void
	{
		this._r *= value._r;
		this._g *= value._g;
		this._b *= value._b;
		this._a *= value._a;
	}

	multiplyThisScalar(value: number): void
	{
		this._r *= value;
		this._g *= value;
		this._b *= value;
		this._a *= value;
	}

	subtract(value: Color): Color
	{
		return new Color(
			this._r - value._r,
			this._g - value._g,
			this._b - value._b,
			this._a - value._a
		);
	}

	subtractFromThis(value: Color): void
	{
		this._r -= value._r;
		this._g -= value._g;
		this._b -= value._b;
		this._a -= value._a;
	}

	clone(): Color
	{
		return new Color(this.r, this.g, this.b, this.a);
	}

	equals(other: Color): Boolean
	{
		return this.r == other.r
			&& this.g == other.g
			&& this.b == other.b
			&& this.a == other.a;
	}

	trim(): void
	{
		if(this._r > 1) this._r = 1;
		if(this._g > 1) this._g = 1;
		if(this._b > 1) this._b = 1;
		if(this._a > 1) this._a = 1;

		if(this._r < 0) this._r = 0;
		if(this._g < 0) this._g = 0;
		if(this._b < 0) this._b = 0;
		if(this._a < 0) this._a = 0;
	}
}