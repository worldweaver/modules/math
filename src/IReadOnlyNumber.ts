export default interface IReadOnlyNumber
{
	get value(): number;
}