import IMathObject from "./IMathObject";
import IReadOnlyRange from "./IReadOnlyRange";

export default class Range<T extends IMathObject<T>> implements IReadOnlyRange<T>
{
	private _min: T;
	private _max: T;

	get min(): T
	{
		return this._min;
	}
	set min(value: T)
	{
		this._min = value;
	}

	get max(): T
	{
		return this._max;
	}
	set max(value: T)
	{
		this._max = value;
	}

	get difference() : T
	{
		return this._max.subtract(this._min);
	}

	constructor(min: T, max: T = min.clone())
	{
		this._min = min;
		this._max = max;
	}

	set(min: T, max: T = min)
	{
		this._min = min;
		this._max = max;
	}

	random() : T
	{
		const minValues = this.min.values;
		const maxValues = this.max.values;
		const result = this.min.clone();

		for(let i=0; i<minValues.length; i++)
		{
			result.setValue(i, minValues[i] + ((maxValues[i] - minValues[i]) * Math.random()));
		}

		return result;
	}
}