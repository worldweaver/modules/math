const JSDOM = require( 'jsdom' ).JSDOM;
const canvas = require( 'canvas' );

const jsdomOptions = {
    url: 'http://localhost/'
};

const jsdomInstance = new JSDOM( '', jsdomOptions );
const { window } = jsdomInstance;

Object.getOwnPropertyNames( window )
    .filter( property => !property.startsWith( '_' ) )
    .forEach( key => global[key] = window[key] );

global.window = window;
window.console = global.console;


global['CanvasRenderingContext2D'] = canvas.Context2d;