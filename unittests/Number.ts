import * as Assert from "assert";
import {it} from "mocha";
import Number from "../src/Number";

describe("Number", ()=>
{
	context("When comparing two Numbers with `equals`", ()=>
	{
		it("true is returned if they have the same values", ()=>
		{
			const number = new Number(5);
			const result = number.equals(new Number(5));
			Assert.ok(result, "The result was false despite the Numbers being equal.");
		})

		it("false is returned if they do not have the same values", ()=>
		{
			const number = new Number(5);

			const result = number.equals(new Number(6));
			Assert.ok(!result, "The result was true despite the number having different values.");
		})
	});

	context("When adding a Number with `add`", () =>
	{
		it("neither Number is altered", () =>
		{
			const numberA = new Number(10);
			const numberB = new Number(5);
			numberA.add(numberB);

			Assert.strictEqual(numberA.value, 10, "The left hand Number value was altered.");
			Assert.strictEqual(numberB.value, 5, "The right hand Number value was altered.");
		});

		it("the expected value is returned", () =>
		{
			const numberA = new Number(10);
			const numberB = new Number(5);
			const result = numberA.add(numberB);

			Assert.strictEqual(result.value, 15, "The result of the addition was not correct.");
		});
	});

	context("When adding a Number with `addToThis`", ()=>
	{
		it("the left hand Number is altered to the result of the addition", () =>
		{
			const number = new Number(10);
			number.addToThis(new Number(5));

			Assert.strictEqual(number.value, 15, "The left hand Number wasn't changed to the expected result.");
		});

		it("the right hand Number is not altered", () =>
		{
			const numberA = new Number(10);
			const numberB = new Number(5);
			numberA.addToThis(numberB);

			Assert.strictEqual(numberB.value, 5, "The right hand Number was altered.");
		});
	});

	context("When subtracting a Number with `subtract`", () =>
	{
		it("neither Number is altered", () =>
		{
			const numberA = new Number(10);
			const numberB = new Number(5);
			numberA.subtract(numberB);

			Assert.strictEqual(numberA.value, 10, "The left hand Number value was altered.");
			Assert.strictEqual(numberB.value, 5, "The right hand Number value was altered.");
		});

		it("the expected value is returned", () =>
		{
			const numberA = new Number(20);
			const numberB = new Number(5);
			const result = numberA.subtract(numberB);

			Assert.strictEqual(result.value, 15, "The result of the subtraction was not correct.");
		});
	});

	context("When subtracting a Number with `subtractFromThis`", ()=>
	{
		it("the left hand Number is altered to the result of the subtraction", () =>
		{
			const number = new Number(20);
			number.subtractFromThis(new Number(5));

			Assert.strictEqual(number.value, 15, "The left hand Number wasn't changed to the expected result.");
		});

		it("the right hand Number is not altered", () =>
		{
			const numberA = new Number(10);
			const numberB = new Number(5);
			numberA.subtractFromThis(numberB);

			Assert.strictEqual(numberB.value, 5, "The right hand Number was altered.");
		});
	});

	context("When multiplying a Number with `multiply`", () =>
	{
		it("neither Number is altered", () =>
		{
			const numberA = new Number(10);
			const numberB = new Number(5);
			numberA.multiply(numberB);

			Assert.strictEqual(numberA.value, 10, "The left hand Number value was altered.");
			Assert.strictEqual(numberB.value, 5, "The right hand Number value was altered.");
		});

		it("the expected value is returned", () =>
		{
			const numberA = new Number(20);
			const numberB = new Number(5);
			const result = numberA.multiply(numberB);

			Assert.strictEqual(result.value, 100, "The result of the multiplication was not correct.");
		});
	});

	context("When multiplying a Number with `multiplyThis`", ()=>
	{
		it("the left hand Number is altered to the result of the multiplication", () =>
		{
			const number = new Number(20);
			number.multiplyThis(new Number(5));

			Assert.strictEqual(number.value, 100, "The left hand Number wasn't changed to the expected result.");
		});

		it("the right hand Number is not altered", () =>
		{
			const numberA = new Number(10);
			const numberB = new Number(5);
			numberA.multiplyThis(numberB);

			Assert.strictEqual(numberB.value, 5, "The right hand Number was altered.");
		});
	});

	context("When multiplying a Number with `multiplyScalar`", () =>
	{
		it("the Number is not altered", () =>
		{
			const number = new Number(10);
			number.multiplyScalar(10);

			Assert.strictEqual(number.value, 10, "The Number value was altered.");
		});

		it("the expected value is returned", () =>
		{
			const number = new Number(20);
			const result = number.multiplyScalar(5);

			Assert.strictEqual(result.value, 100, "The result of the scalar multiplication was not correct.");
		});
	});

	context("When multiplying a Number with `multiplyThisScalar`", ()=>
	{
		it("the Number is altered to the result of the scalar multiplication", () =>
		{
			const number = new Number(20);
			number.multiplyThisScalar(5);

			Assert.strictEqual(number.value, 100, "The Number wasn't changed to the expected result.");
		});
	});

	context("When dividing a Number with `divide`", () =>
	{
		it("neither Number is altered", () =>
		{
			const numberA = new Number(10);
			const numberB = new Number(5);
			numberA.divide(numberB);

			Assert.strictEqual(numberA.value, 10, "The left hand Number value was altered.");
			Assert.strictEqual(numberB.value, 5, "The right hand Number value was altered.");
		});

		it("the expected value is returned", () =>
		{
			const numberA = new Number(20);
			const numberB = new Number(5);
			const result = numberA.divide(numberB);

			Assert.strictEqual(result.value, 4, "The result of the division was not correct.");
		});
	});

	context("When dividing a Number with `divideThis`", ()=>
	{
		it("the left hand Number is altered to the result of the division", () =>
		{
			const number = new Number(20);
			number.divideThis(new Number(5));

			Assert.strictEqual(number.value, 4, "The left hand Number wasn't changed to the expected result.");
		});

		it("the right hand Number is not altered", () =>
		{
			const numberA = new Number(10);
			const numberB = new Number(5);
			numberA.divideThis(numberB);

			Assert.strictEqual(numberB.value, 5, "The right hand Number was altered.");
		});
	});

	context("When dividing a Number with `divideScalar`", () =>
	{
		it("the Number is not altered", () =>
		{
			const number = new Number(10);
			number.divideScalar(10);

			Assert.strictEqual(number.value, 10, "The Number value was altered.");
		});

		it("the expected value is returned", () =>
		{
			const number = new Number(20);
			const result = number.divideScalar(5);

			Assert.strictEqual(result.value, 4, "The result of the scalar division was not correct.");
		});
	});

	context("When dividing a Number with `divideThisScalar`", ()=>
	{
		it("the Number is altered to the result of the scalar division", () =>
		{
			const number = new Number(20);
			number.divideThisScalar(5);

			Assert.strictEqual(number.value, 4, "The Number wasn't changed to the expected result.");
		});
	});

	context("When setting the value of a Number with `setValue`", ()=>
	{
		it("index 0 sets the value", () =>
		{
			const number = new Number(5);
			number.setValue(0, 10);

			Assert.strictEqual(number.value, 10, "The Number value wasn't changed to the given value.");
		});

		it("values beside 0 throws an error", () =>
		{
			const number = new Number(5);
			Assert.throws(()=>
			{
				number.setValue(-1, 10);
			}, "No error was thrown for an invalid index.");
			Assert.throws(()=>
			{
				number.setValue(1, 10);
			}, "No error was thrown for an invalid index.");
		});
	});

	context("When setting the value of a Number with `values`", ()=>
	{
		it("the value is assigned the 0 index of the array", ()=>
		{
			const number = new Number(0);
			number.values = [5];

			Assert.strictEqual(number.value, 5, "The value was not assigned to the 0 index of the array.");
		});
	});

	context("When getting the values of a Number with `values`", ()=>
	{
		it("the 0 index of the array is assigned the value", ()=>
		{
			const number = new Number(5);
			const array = number.values;

			Assert.strictEqual(array[0], number.value, "The 0 index of the array was not assigned to the value.");
		});

		it("the array size is 1", ()=>
		{
			const number = new Number(5);

			Assert.strictEqual(number.values.length, 1, "The size of the array was not 1.");
		});
	});

	context("When cloning a Number", ()=>
	{
		it("the new Number has the same values as the one cloned", () =>
		{
			const numberA = new Number(20);
			const numberB = numberA.clone();

			Assert.ok(numberB.equals(numberA), "The cloned Number didn't have the same value as the original.");
		});

		it("the new Number is a different Number reference", () =>
		{
			const numberA = new Number(20);
			const numberB = numberA.clone();

			Assert.notStrictEqual(numberA, numberB, "The cloned Number is the same reference as the original.");
		});
	});

	context("When creating a default Number", ()=>
	{
		it("the value of the Number is 0", () =>
		{
			const number = new Number();

			Assert.strictEqual(number.value, 0, "The value of a default Number should be 0.");
		});
	});
});