import * as Assert from "assert";
import {it} from "mocha";
import Vector2 from "../src/Vector2";

describe("Vector2", ()=>
{
	context("When comparing two Vector2 with `equals`", ()=>
	{
		it("true is returned if they have the same values for x and y", ()=>
		{
			const vector = new Vector2(5,5);
			const result = vector.equals(new Vector2(5,5));
			Assert.ok(result, "The result was false despite the Vector2s being equal.");
		})

		it("false is returned if they do not have the same values for x and y", ()=>
		{
			const vector = new Vector2(5, 5);

			let result = vector.equals(new Vector2(6,5));
			Assert.ok(!result, "The result was true despite the vectors having a different x value.");

			result = vector.equals(new Vector2(5,6));
			Assert.ok(!result, "The result was true despite the vectors having a different y value.");

			result = vector.equals(new Vector2(6,6));
			Assert.ok(!result, "The result was true despite the vectors having a different x and y value.");
		})
	});

	context("When adding a Vector2 with `add`", () =>
	{
		it("neither Vector2 is altered", () =>
		{
			const vectorA = new Vector2(10,10);
			const vectorB = new Vector2(5,5);
			vectorA.add(vectorB);

			Assert.ok(vectorA.equals(new Vector2(10,10)), "The left hand Vector2 values were altered.");
			Assert.ok(vectorB.equals(new Vector2(5,5)), "The right hand Vector2 values were altered.");
		});

		it("the expected value is returned", () =>
		{
			const vectorA = new Vector2(10,10);
			const vectorB = new Vector2(5,5);
			const result = vectorA.add(vectorB);

			Assert.ok(result.equals(new Vector2(15,15)), "The result of the addition was not correct.");
		});
	});

	context("When adding a Vector2 with `addToThis`", ()=>
	{
		it("the left hand Vector2 is altered to the result of the addition", () =>
		{
			const vector = new Vector2(10,10);
			vector.addToThis(new Vector2(5, 5));

			Assert.ok(vector.equals(new Vector2(15, 15)), "The left hand Vector2 wasn't changed to the expected result.");
		});

		it("the right hand Vector2 is not altered", () =>
		{
			const vectorA = new Vector2(10,10);
			const vectorB = new Vector2(5,5);
			vectorA.addToThis(vectorB);

			Assert.ok(vectorB.equals(new Vector2(5, 5)), "The right hand Vector2 was altered.");
		});
	});

	context("When subtracting a Vector2 with `subtract`", () =>
	{
		it("neither Vector2 is altered", () =>
		{
			const vectorA = new Vector2(10,10);
			const vectorB = new Vector2(5,5);
			vectorA.subtract(vectorB);

			Assert.ok(vectorA.equals(new Vector2(10,10)), "The left hand Vector2 values were altered.");
			Assert.ok(vectorB.equals(new Vector2(5,5)), "The right hand Vector2 values were altered.");
		});

		it("the expected value is returned", () =>
		{
			const vectorA = new Vector2(20,20);
			const vectorB = new Vector2(5,5);
			const result = vectorA.subtract(vectorB);

			Assert.ok(result.equals(new Vector2(15,15)), "The result of the subtraction was not correct.");
		});
	});

	context("When subtracting a Vector2 with `subtractFromThis`", ()=>
	{
		it("the left hand Vector2 is altered to the result of the subtraction", () =>
		{
			const vector = new Vector2(20,20);
			vector.subtractFromThis(new Vector2(5,5));

			Assert.ok(vector.equals(new Vector2(15,15)), "The left hand Vector2 wasn't changed to the expected result.");
		});

		it("the right hand Vector2 is not altered", () =>
		{
			const vectorA = new Vector2(10,10);
			const vectorB = new Vector2(5,5);
			vectorA.subtractFromThis(vectorB);

			Assert.ok(vectorB.equals(new Vector2(5,5)), "The right hand Vector2 was altered.");
		});
	});

	context("When multiplying a Vector2 with `multiply`", () =>
	{
		it("neither Vector2 is altered", () =>
		{
			const vectorA = new Vector2(10,10);
			const vectorB = new Vector2(5,5);
			vectorA.multiply(vectorB);

			Assert.ok(vectorA.equals(new Vector2(10,10)), "The left hand Vector2 values were altered.");
			Assert.ok(vectorB.equals(new Vector2(5,5)), "The right hand Vector2 values were altered.");
		});

		it("the expected value is returned", () =>
		{
			const vectorA = new Vector2(20,20);
			const vectorB = new Vector2(5,5);
			const result = vectorA.multiply(vectorB);

			Assert.ok(result.equals(new Vector2(100,100)), "The result of the multiplication was not correct.");
		});
	});

	context("When multiplying a Vector2 with `multiplyThis`", ()=>
	{
		it("the left hand Vector2 is altered to the result of the multiplication", () =>
		{
			const vector = new Vector2(20,20);
			vector.multiplyThis(new Vector2(5,5));

			Assert.ok(vector.equals(new Vector2(100,100)), "The left hand Vector2 wasn't changed to the expected result.");
		});

		it("the right hand Vector2 is not altered", () =>
		{
			const vectorA = new Vector2(10,10);
			const vectorB = new Vector2(5,5);
			vectorA.multiplyThis(vectorB);

			Assert.ok(vectorB.equals(new Vector2(5,5)), "The right hand Vector2 was altered.");
		});
	});

	context("When multiplying a Vector2 with `multiplyScalar`", () =>
	{
		it("the Vector2 is not altered", () =>
		{
			const vector = new Vector2(10,10);
			vector.multiplyScalar(10);

			Assert.ok(vector.equals(new Vector2(10,10)), "The Vector2 values were altered.");
		});

		it("the expected value is returned", () =>
		{
			const vector = new Vector2(20,20);
			const result = vector.multiplyScalar(5);

			Assert.ok(result.equals(new Vector2(100,100)), "The result of the scalar multiplication was not correct.");
		});
	});

	context("When multiplying a Vector2 with `multiplyThisScalar`", ()=>
	{
		it("the vector2 is altered to the result of the scalar multiplication", () =>
		{
			const vector = new Vector2(20,20);
			vector.multiplyThisScalar(5);

			Assert.ok(vector.equals(new Vector2(100, 100)), "The Vector2 wasn't changed to the expected result.");
		});
	});

	context("When dividing a Vector2 with `divide`", () =>
	{
		it("neither Vector2 is altered", () =>
		{
			const vectorA = new Vector2(10,10);
			const vectorB = new Vector2(5,5);
			vectorA.divide(vectorB);

			Assert.ok(vectorA.equals(new Vector2(10,10)), "The left hand Vector2 values were altered.");
			Assert.ok(vectorB.equals(new Vector2(5,5)), "The right hand Vector2 values were altered.");
		});

		it("the expected value is returned", () =>
		{
			const vectorA = new Vector2(20,20);
			const vectorB = new Vector2(5,5);
			const result = vectorA.divide(vectorB);

			Assert.ok(result.equals(new Vector2(4,4)), "The result of the division was not correct.");
		});
	});

	context("When dividing a Vector2 with `divideThis`", ()=>
	{
		it("the left hand Vector2 is altered to the result of the division", () =>
		{
			const vector = new Vector2(20,20);
			vector.divideThis(new Vector2(5,5));

			Assert.ok(vector.equals(new Vector2(4,4)), "The left hand Vector2 wasn't changed to the expected result.");
		});

		it("the right hand Vector2 is not altered", () =>
		{
			const vectorA = new Vector2(10,10);
			const vectorB = new Vector2(5,5);
			vectorA.divideThis(vectorB);

			Assert.ok(vectorB.equals(new Vector2(5,5)), "The right hand Vector2 was altered.");
		});
	});

	context("When dividing a Vector2 with `divideScalar`", () =>
	{
		it("the vector is not altered", () =>
		{
			const vector = new Vector2(10,10);
			vector.divideScalar(10);

			Assert.ok(vector.equals(new Vector2(10,10)), "The Vector2 values were altered.");
		});

		it("the expected value is returned", () =>
		{
			const vector = new Vector2(20,20);
			const result = vector.divideScalar(5);

			Assert.ok(result.equals(new Vector2(4,4)), "The result of the scalar division was not correct.");
		});
	});

	context("When dividing a Vector2 with `divideThisScalar`", ()=>
	{
		it("the vector2 is altered to the result of the scalar division", () =>
		{
			const vector = new Vector2(20,20);
			vector.divideThisScalar(5);

			Assert.ok(vector.equals(new Vector2(4, 4)), "The Vector2 wasn't changed to the expected result.");
		});
	});

	context("When setting the values of a Vector2 with `setValue`", ()=>
	{
		it("index 0 sets the x value", () =>
		{
			const vector = new Vector2(5, 5);
			vector.setValue(0, 10);

			Assert.strictEqual(vector.x, 10, "The Vector2 x value wasn't changed to the given value.");
			Assert.strictEqual(vector.y, 5, "The Vector2 y value was changed.");
		});

		it("index 1 sets the y value", () =>
		{
			const vector = new Vector2(5, 5);
			vector.setValue(1, 10);

			Assert.strictEqual(vector.x, 5, "The Vector2 x value was changed.");
			Assert.strictEqual(vector.y, 10, "The Vector2 y value wasn't changed to the given value.");
		});

		it("values besides 0 and 1 throws an error", () =>
		{
			const vector = new Vector2(5, 5);
			Assert.throws(()=>
			{
				vector.setValue(-1, 10);
			}, "No error was thrown for an invalid index.");
			Assert.throws(()=>
			{
				vector.setValue(3, 10);
			}, "No error was thrown for an invalid index.");
		});
	});

	context("When setting the values of a Vector2 with `values`", ()=>
	{
		it("the x value is assigned the 0 index of the array", ()=>
		{
			const vector = new Vector2(0,0);
			vector.values = [5,8];

			Assert.strictEqual(vector.x, 5, "The x value was not assigned to the 0 index of the array.");
		});

		it("the y value is assigned the 1 index of the array", ()=>
		{
			const vector = new Vector2(0,0);
			vector.values = [5,8];

			Assert.strictEqual(vector.y, 8, "The y value was not assigned to the 1 index of the array.");
		});
	});

	context("When getting the values of a Vector2 with `values`", ()=>
	{
		it("the 0 index of the array is assigned the x value", ()=>
		{
			const vector = new Vector2(5,8);
			const array = vector.values;

			Assert.strictEqual(array[0], vector.x, "The 0 index of the array was not assigned to the x value.");
		});

		it("the 1 index of the array is assigned the y value", ()=>
		{
			const vector = new Vector2(5,8);
			const array = vector.values;

			Assert.strictEqual(array[1], vector.y, "The 1 index of the array was not assigned to the y value.");
		});

		it("the array size is 2", ()=>
		{
			const vector = new Vector2(5,8);

			Assert.strictEqual(vector.values.length, 2, "The size of the array was not 2.");
		});
	});

	context("When cloning a Vector2", ()=>
	{
		it("the new Vector2 has the same values as the one cloned", () =>
		{
			const vectorA = new Vector2(20,20);
			const vectorB = vectorA.clone();

			Assert.ok(vectorB.equals(vectorA), "The cloned Vector2 didn't have the same values as the original.");
		});

		it("the new Vector2 is a different Vector2 reference", () =>
		{
			const vectorA = new Vector2(20,20);
			const vectorB = vectorA.clone();

			Assert.notStrictEqual(vectorA, vectorB, "The cloned Vector2 is the same reference as the original.");
		});
	});

	context("When creating a default Vector2", ()=>
	{
		it("the values of the Vector2 are both 0", () =>
		{
			const vector = new Vector2();

			Assert.strictEqual(vector.x, 0, "The x value of a default Vector2 should be 0.")
			Assert.strictEqual(vector.y, 0, "The y value of a default Vector2 should be 0.")
		});
	});

	context("When converting to a Point", ()=>
	{
		it("the values of the Vector2 match that of the Point", () =>
		{
			const vector = new Vector2(3, 4);
			const point = vector.asPoint;

			Assert.strictEqual(point.x, vector.x, "The Point didn't have the same x value as the Vector2.")
			Assert.strictEqual(point.y, vector.y, "The Point didn't have the same y value as the Vector2.")
		});
	});
});