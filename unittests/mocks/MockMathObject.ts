import {IMathObject} from "../../src";

export default class MockMathObject implements IMathObject<MockMathObject>
{
	private readonly _values: Array<number>;

	get values(): Array<number>
	{
		return this._values;
	}

	constructor(...values: Array<number>)
	{
		this._values = values;
	}

	setValue(index: number, value: number): void
	{
		this._values[index] = value;
	}

	add(value: MockMathObject): MockMathObject
	{
		const result = this.clone();
		result.addToThis(value);
		return result;
	}

	addToThis(value: MockMathObject): void
	{
		for(let i=0; i<this._values.length; i++)
		{
			this._values[i] += value.values[i];
		}
	}

	subtract(value: MockMathObject): MockMathObject
	{
		const result = this.clone();
		result.subtractFromThis(value);
		return result;
	}

	subtractFromThis(value: MockMathObject): void
	{
		for(let i=0; i<this._values.length; i++)
		{
			this._values[i] -= value.values[i];
		}
	}

	multiply(value: MockMathObject): MockMathObject
	{
		const result = this.clone();
		result.multiplyThis(value);
		return result;
	}

	multiplyScalar(value: number): MockMathObject
	{
		const result = this.clone();
		result.multiplyThisScalar(value);
		return result;
	}

	multiplyThis(value: MockMathObject): void
	{
		for(let i=0; i<this._values.length; i++)
		{
			this._values[i] *= value.values[i];
		}
	}

	multiplyThisScalar(value: number): void
	{
		for(let i=0; i<this._values.length; i++)
		{
			this._values[i] *= value;
		}
	}

	divide(value: MockMathObject): MockMathObject
	{
		const result = this.clone();
		result.divideThis(value);
		return result;
	}

	divideScalar(value: number): MockMathObject
	{
		const result = this.clone();
		result.divideThisScalar(value);
		return result;
	}

	divideThis(value: MockMathObject): void
	{
		for(let i=0; i<this._values.length; i++)
		{
			this._values[i] /= value.values[i];
		}
	}

	divideThisScalar(value: number): void
	{
		for(let i=0; i<this._values.length; i++)
		{
			this._values[i] /= value;
		}
	}

	equals(other: MockMathObject): Boolean
	{
		return this._values.every((value: number, index: number): value is number =>
		{
			return value === other.values[index];
		});
	}

	clone(): MockMathObject
	{
		const cloned = new MockMathObject();
		this._values.forEach((value: number, index: number)=>cloned.setValue(index, value));

		return cloned;
	}
}