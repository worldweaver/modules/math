import * as Assert from "assert";
import {it} from "mocha";
import Color from "../src/Color";

describe("Color", ()=>
{
	context("When comparing two Color with `equals`", ()=>
	{
		it("true is returned if they have the same values for r, g, b, and a", ()=>
		{
			const color = new Color(0.2, 0.4, 0.6, 1);
			const result = color.equals(new Color(0.2, 0.4, 0.6, 1));
			Assert.ok(result, "The result was false despite the Colors being equal.");
		})

		it("false is returned if they do not have the same values for r, g, b, and a", ()=>
		{
			const color = new Color(0.2, 0.4, 0.6, 1);

			let result = color.equals(new Color(0.3, 0.4, 0.6, 1));
			Assert.ok(!result, "The result was true despite the colors having a different r value.");

			result = color.equals(new Color(0.2, 0.5, 0.6, 1));
			Assert.ok(!result, "The result was true despite the colors having a different g value.");

			result = color.equals(new Color(0.2, 0.4, 0.7, 1));
			Assert.ok(!result, "The result was true despite the colors having a different b value.");

			result = color.equals(new Color(0.2, 0.4, 0.6, 0.5));
			Assert.ok(!result, "The result was true despite the colors having a different a value.");

			result = color.equals(new Color(1, 1, 1, 0.5));
			Assert.ok(!result, "The result was true despite the colors having a different r, g, b and a value.");
		})
	});

	context("When adding a Color with `add`", () =>
	{
		it("neither Color is altered", () =>
		{
			const colorA = new Color(0.5,0.5,0.5,0.5);
			const colorB = new Color(0.2,0.2,0.2,0.2);
			colorA.add(colorB);

			Assert.ok(colorA.equals(new Color(0.5,0.5,0.5,0.5)), "The left hand Color values were altered.");
			Assert.ok(colorB.equals(new Color(0.2,0.2,0.2,0.2)), "The right hand Color values were altered.");
		});

		it("the expected value is returned", () =>
		{
			const colorA = new Color(0.5,0.5,0.5,0.5);
			const colorB = new Color(0.2,0.2,0.2,0.2);
			const result = colorA.add(colorB);

			Assert.ok(result.equals(new Color(0.7,0.7,0.7,0.7)), "The result of the addition was not correct.");
		});
	});

	context("When adding a Color with `addToThis`", ()=>
	{
		it("the left hand Color is altered to the result of the addition", () =>
		{
			const color = new Color(0.5,0.5,0.5,0.5);
			color.addToThis(new Color(0.2,0.2,0.2,0.2));

			Assert.ok(color.equals(new Color(0.7,0.7,0.7,0.7)), "The left hand Color wasn't changed to the expected result.");
		});

		it("the right hand Color is not altered", () =>
		{
			const colorA = new Color(0.5,0.5,0.5,0.5);
			const colorB = new Color(0.2,0.2,0.2,0.2);
			colorA.addToThis(colorB);

			Assert.ok(colorB.equals(new Color(0.2,0.2,0.2,0.2)), "The right hand Color was altered.");
		});
	});

	context("When subtracting a Color with `subtract`", () =>
	{
		it("neither Color is altered", () =>
		{
			const colorA = new Color(0.5,0.5,0.5,0.5);
			const colorB = new Color(0.2,0.2,0.2,0.2);
			colorA.subtract(colorB);

			Assert.ok(colorA.equals(new Color(0.5,0.5,0.5,0.5)), "The left hand Color values were altered.");
			Assert.ok(colorB.equals(new Color(0.2,0.2,0.2,0.2)), "The right hand Color values were altered.");
		});

		it("the expected value is returned", () =>
		{
			const colorA = new Color(0.5,0.5,0.5,0.5);
			const colorB = new Color(0.2,0.2,0.2,0.2);
			const result = colorA.subtract(colorB);

			Assert.ok(result.equals(new Color(0.3,0.3,0.3,0.3)), "The result of the subtraction was not correct.");
		});
	});

	context("When subtracting a Color with `subtractFromThis`", ()=>
	{
		it("the left hand Color is altered to the result of the subtraction", () =>
		{
			const color = new Color(0.5,0.5,0.5,0.5);
			color.subtractFromThis(new Color(0.2,0.2,0.2,0.2));

			Assert.ok(color.equals(new Color(0.3,0.3,0.3,0.3)), "The left hand Color wasn't changed to the expected result.");
		});

		it("the right hand Color is not altered", () =>
		{
			const colorA = new Color(0.5,0.5,0.5,0.5);
			const colorB = new Color(0.2,0.2,0.2,0.2);
			colorA.subtractFromThis(colorB);

			Assert.ok(colorB.equals(new Color(0.2,0.2,0.2,0.2)), "The right hand Color was altered.");
		});
	});

	context("When multiplying a Color with `multiply`", () =>
	{
		it("neither Color is altered", () =>
		{
			const colorA = new Color(0.5,0.5,0.5,0.5);
			const colorB = new Color(0.2,0.2,0.2,0.2);
			colorA.multiply(colorB);

			Assert.ok(colorA.equals(new Color(0.5,0.5,0.5,0.5)), "The left hand Color values were altered.");
			Assert.ok(colorB.equals(new Color(0.2,0.2,0.2,0.2)), "The right hand Color values were altered.");
		});

		it("the expected value is returned", () =>
		{
			const colorA = new Color(0.5,0.5,0.5,0.5);
			const colorB = new Color(2,2,2,2);
			const result = colorA.multiply(colorB);

			Assert.ok(result.equals(new Color(1,1,1,1)), "The result of the multiplication was not correct.");
		});
	});

	context("When multiplying a Color with `multiplyThis`", ()=>
	{
		it("the left hand Color is altered to the result of the multiplication", () =>
		{
			const color = new Color(0.5,0.5,0.5,0.5);
			color.multiplyThis(new Color(2,2,2,2));

			Assert.ok(color.equals(new Color(1,1,1,1)), "The left hand Color wasn't changed to the expected result.");
		});

		it("the right hand Color is not altered", () =>
		{
			const colorA = new Color(0.5,0.5,0.5,0.5);
			const colorB = new Color(0.2,0.2,0.2,0.2);
			colorA.multiplyThis(colorB);

			Assert.ok(colorB.equals(new Color(0.2,0.2,0.2,0.2)), "The right hand Color was altered.");
		});
	});

	context("When multiplying a Color with `multiplyScalar`", () =>
	{
		it("the Color is not altered", () =>
		{
			const color = new Color(0.5,0.5,0.5,0.5);
			color.multiplyScalar(2);

			Assert.ok(color.equals(new Color(0.5,0.5,0.5,0.5)), "The Color values were altered.");
		});

		it("the expected value is returned", () =>
		{
			const color = new Color(0.5,0.5,0.5,0.5);
			const result = color.multiplyScalar(2);

			Assert.ok(result.equals(new Color(1,1,1,1)), "The result of the scalar multiplication was not correct.");
		});
	});

	context("When multiplying a Color with `multiplyThisScalar`", ()=>
	{
		it("the color is altered to the result of the scalar multiplication", () =>
		{
			const color = new Color(0.5,0.5,0.5,0.5);
			color.multiplyThisScalar(2);

			Assert.ok(color.equals(new Color(1,1,1,1)), "The Color wasn't changed to the expected result.");
		});
	});

	context("When dividing a Color with `divide`", () =>
	{
		it("neither Color is altered", () =>
		{
			const colorA = new Color(1,1,1,1);
			const colorB = new Color(0.2,0.2,0.2,0.2);
			colorA.divide(colorB);

			Assert.ok(colorA.equals(new Color(1,1,1,1)), "The left hand Color values were altered.");
			Assert.ok(colorB.equals(new Color(0.2,0.2,0.2,0.2)), "The right hand Color values were altered.");
		});

		it("the expected value is returned", () =>
		{
			const colorA = new Color(1,1,1,1);
			const colorB = new Color(2,2,2,2);
			const result = colorA.divide(colorB);

			Assert.ok(result.equals(new Color(0.5,0.5,0.5,0.5)), "The result of the division was not correct.");
		});
	});

	context("When dividing a Color with `divideThis`", ()=>
	{
		it("the left hand Color is altered to the result of the division", () =>
		{
			const color = new Color(1,1,1,1);
			color.divideThis(new Color(2,2,2,2));

			Assert.ok(color.equals(new Color(0.5,0.5,0.5,0.5)), "The left hand Color wasn't changed to the expected result.");
		});

		it("the right hand Color is not altered", () =>
		{
			const colorA = new Color(1,1,1,1);
			const colorB = new Color(0.2,0.2,0.2,0.2);
			colorA.divideThis(colorB);

			Assert.ok(colorB.equals(new Color(0.2,0.2,0.2,0.2)), "The right hand Color was altered.");
		});
	});

	context("When dividing a Color with `divideScalar`", () =>
	{
		it("the color is not altered", () =>
		{
			const color = new Color(1,1,1,1);
			color.divideScalar(2);

			Assert.ok(color.equals(new Color(1,1,1,1)), "The Color values were altered.");
		});

		it("the expected value is returned", () =>
		{
			const color = new Color(1,1,1,1);
			const result = color.divideScalar(2);

			Assert.ok(result.equals(new Color(0.5,0.5,0.5,0.5)), "The result of the scalar division was not correct.");
		});
	});

	context("When dividing a Color with `divideThisScalar`", ()=>
	{
		it("the color is altered to the result of the scalar division", () =>
		{
			const color = new Color(1,1,1,1);
			color.divideThisScalar(2);

			Assert.ok(color.equals(new Color(0.5,0.5,0.5,0.5)), "The Color wasn't changed to the expected result.");
		});
	});

	context("When setting the values of a Color with `setValue`", ()=>
	{
		it("index 0 sets the r value", () =>
		{
			const color = new Color(0.2,0.3,0.4,0.5);
			color.setValue(0, 0.8);

			Assert.strictEqual(color.r, 0.8, "The Color r value wasn't changed to the given value.");
			Assert.strictEqual(color.g, 0.3, "The Color g value was changed.");
			Assert.strictEqual(color.b, 0.4, "The Color b value was changed.");
			Assert.strictEqual(color.a, 0.5, "The Color a value was changed.");
		});

		it("index 1 sets the g value", () =>
		{
			const color = new Color(0.2,0.3,0.4,0.5);
			color.setValue(1, 0.8);

			Assert.strictEqual(color.r, 0.2, "The Color r value was changed.");
			Assert.strictEqual(color.g, 0.8, "The Color g value wasn't changed to the given value.");
			Assert.strictEqual(color.b, 0.4, "The Color b value was changed.");
			Assert.strictEqual(color.a, 0.5, "The Color a value was changed.");
		});

		it("index 2 sets the b value", () =>
		{
			const color = new Color(0.2,0.3,0.4,0.5);
			color.setValue(2, 0.8);

			Assert.strictEqual(color.r, 0.2, "The Color r value was changed.");
			Assert.strictEqual(color.g, 0.3, "The Color g value was changed.");
			Assert.strictEqual(color.b, 0.8, "The Color b value wasn't changed to the given value.");
			Assert.strictEqual(color.a, 0.5, "The Color a value was changed.");
		});

		it("index 3 sets the a value", () =>
		{
			const color = new Color(0.2,0.3,0.4,0.5);
			color.setValue(3, 0.8);

			Assert.strictEqual(color.r, 0.2, "The Color r value was changed.");
			Assert.strictEqual(color.g, 0.3, "The Color g value was changed.");
			Assert.strictEqual(color.b, 0.4, "The Color b value was changed.");
			Assert.strictEqual(color.a, 0.8, "The Color a value wasn't changed to the given value.");
		});

		it("values besides 0, 1, 2 or 3 throws an error", () =>
		{
			const color = new Color(0.5,0.5,0.5,0.5);
			Assert.throws(()=>
			{
				color.setValue(-1, 1);
			}, "No error was thrown for an invalid index.");
			Assert.throws(()=>
			{
				color.setValue(4, 1);
			}, "No error was thrown for an invalid index.");
		});
	});

	context("When setting the values of a Color with `values`", ()=>
	{
		it("the r value is assigned the 0 index of the array", ()=>
		{
			const color = new Color(0,0,0,0);
			color.values = [0.1,0.2,0.3,0.4];

			Assert.strictEqual(color.r, 0.1, "The r value was not assigned to the 0 index of the array.");
		});

		it("the g value is assigned the 1 index of the array", ()=>
		{
			const color = new Color(0,0,0,0);
			color.values = [0.1,0.2,0.3,0.4];

			Assert.strictEqual(color.g, 0.2, "The g value was not assigned to the 1 index of the array.");
		});

		it("the b value is assigned the 2 index of the array", ()=>
		{
			const color = new Color(0,0,0,0);
			color.values = [0.1,0.2,0.3,0.4];

			Assert.strictEqual(color.b, 0.3, "The b value was not assigned to the 2 index of the array.");
		});

		it("the a value is assigned the 3 index of the array", ()=>
		{
			const color = new Color(0,0,0,0);
			color.values = [0.1,0.2,0.3,0.4];

			Assert.strictEqual(color.a, 0.4, "The a value was not assigned to the 3 index of the array.");
		});
	});

	context("When getting the values of a Color with `values`", ()=>
	{
		it("the 0 index of the array is assigned the r value", ()=>
		{
			const color = new Color(0.1,0.2,0.3,0.4);
			const array = color.values;

			Assert.strictEqual(array[0], color.r, "The 0 index of the array was not assigned to the r value.");
		});

		it("the 1 index of the array is assigned the g value", ()=>
		{
			const color = new Color(0.1,0.2,0.3,0.4);
			const array = color.values;

			Assert.strictEqual(array[1], color.g, "The 1 index of the array was not assigned to the g value.");
		});

		it("the 2 index of the array is assigned the b value", ()=>
		{
			const color = new Color(0.1,0.2,0.3,0.4);
			const array = color.values;

			Assert.strictEqual(array[2], color.b, "The 2 index of the array was not assigned to the b value.");
		});

		it("the 3 index of the array is assigned the a value", ()=>
		{
			const color = new Color(0.1,0.2,0.3,0.4);
			const array = color.values;

			Assert.strictEqual(array[3], color.a, "The 3 index of the array was not assigned to the a value.");
		});

		it("the array size is 3", ()=>
		{
			const color = new Color(1,1,1,1);

			Assert.strictEqual(color.values.length, 4, "The size of the array was not 4.");
		});
	});

	context("When cloning a Color", ()=>
	{
		it("the new Color has the same values as the one cloned", () =>
		{
			const colorA = new Color(0.4,0.5,0.6,0.7);
			const colorB = colorA.clone();

			Assert.ok(colorB.equals(colorA), "The cloned Color didn't have the same values as the original.");
		});

		it("the new Color is a different Color reference", () =>
		{
			const colorA = new Color(0.4,0.5,0.6,0.7);
			const colorB = colorA.clone();

			Assert.notStrictEqual(colorA, colorB, "The cloned Color is the same reference as the original.");
		});
	});

	context("When creating a default Color", ()=>
	{
		it("the values of the Color are all 1", () =>
		{
			const color = new Color();

			Assert.strictEqual(color.r, 1, "The r value of a default Color should be 1.");
			Assert.strictEqual(color.g, 1, "The g value of a default Color should be 1.");
			Assert.strictEqual(color.b, 1, "The b value of a default Color should be 1.");
			Assert.strictEqual(color.a, 1, "The a value of a default Color should be 1.");
		});
	});

	context("When trimming a Color using `trim`", ()=>
	{
		it("values over 1 are trimmed to a value of 1", ()=>
		{
			const color = new Color(2,2,2,2);
			color.trim();
			Assert.ok(color.equals(new Color(1,1,1,1)), "The values of the color were not trimmed to 1.");
		});

		it("values less than 0 are trimmed to a value of 0", ()=>
		{
			const color = new Color(-2,-2,-2,-2);
			color.trim();
			Assert.ok(color.equals(new Color(0,0,0,0)), "The values of the color were not trimmed to 0.");
		});
	});
});