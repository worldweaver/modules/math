import {it} from "mocha";
import {Range} from "../src";
import * as Assert from "assert";
import MockMathObject from "./mocks/MockMathObject";

describe("Range", ()=>
{
	context("When calculating the difference of the Range", ()=>
	{
		it("the expected value is returned", ()=>
		{
			const range = new Range(new MockMathObject(5), new MockMathObject(12));
			Assert.strictEqual(range.difference.values[0], 7, "The difference between 5 and 12 should be 7.");
		});

		it("the expected value is returned when the max is lower than min", ()=>
		{
			const range = new Range(new MockMathObject(12), new MockMathObject(5));
			Assert.strictEqual(range.difference.values[0], -7, "The difference between 12 and 5 should be -7.");
		});
	});

	context("When calculating a random value", ()=>
	{
		it("the result is within the values of the Range", () =>
		{
			const range = new Range(new MockMathObject(5), new MockMathObject(15));
			for(let i=0; i<1000; i++)
			{
				const result = range.random();
				Assert.ok(result.values[0] >= 5, "Value was less than the minimum value of the range.")
				Assert.ok(result.values[0] < 15, "Value was greater than the maximum value of the range.")
			}
		});

		it("the result should be consistent with the random values used", () =>
		{
			setRandomReturnValues(0, 0.5, 1);

			const range = new Range(new MockMathObject(5), new MockMathObject(15));
			Assert.strictEqual(range.random().values[0], 5, "With a random value of 0 the result should be 5.");
			Assert.strictEqual(range.random().values[0], 10, "With a random value of 0.5 the result should be 10.");
			Assert.strictEqual(range.random().values[0], 15, "With a random value of 1 the result should be 15.");
		});

		it("each `value` of the object in the range is calculated as a separate random value", () =>
		{
			setRandomReturnValues(0, 0.2, 0.5);
			const range = new Range(new MockMathObject(5, 0, 50), new MockMathObject(10, 100, 30));

			const randomResult: MockMathObject = range.random();

			Assert.strictEqual(randomResult.values[0], 5, "With a random value of 0 the first random value should be 5.");
			Assert.strictEqual(randomResult.values[1], 20, "With a random value of 0.2 the second random value should be 20.");
			Assert.strictEqual(randomResult.values[2], 40, "With a random value of 0.5 the third random value should be 15.");
		});
	});

	context("When setting the values with `set`", ()=>
	{
		it("the values are updated as expected", () =>
		{
			const range = new Range(new MockMathObject(5), new MockMathObject(10));
			range.set(new MockMathObject(20), new MockMathObject(100));

			Assert.strictEqual(range.min.values[0], 20, "The minimum value was not updated to the correct value.");
			Assert.strictEqual(range.max.values[0], 100, "The maximum value was not updated to the correct value.");
		});
	});

	context("When setting the values through the properties", ()=>
	{
		it("the minimum value updates as expected", () =>
		{
			const range = new Range(new MockMathObject(5), new MockMathObject(10));
			range.min = new MockMathObject(20);

			Assert.strictEqual(range.min.values[0], 20, "The minimum value was not updated to the correct value.");
		});

		it("the maximum value updates as expected", () =>
		{
			const range = new Range(new MockMathObject(5), new MockMathObject(10));
			range.max = new MockMathObject(100);

			Assert.strictEqual(range.max.values[0], 100, "The maximum value was not updated to the correct value.");
		});
	});

	context("When setting the values through the constructor", ()=>
	{
		it("providing only one value doesn't set the same reference to min and max.", () =>
		{
			const range = new Range(new MockMathObject(5));

			Assert.strictEqual(range.min.values[0], 5, "The min should have been set to 5.");
			Assert.strictEqual(range.max.values[0], 5, "The max value should also have been set to 5 as no max was provided.");

			range.max.setValue(0, 42);

			Assert.strictEqual(range.min.values[0], 5, "The minimum value should remain 5.");
			Assert.strictEqual(range.max.values[0], 42, "The maximum value should have changed to 42.");
		});
	});
});

function setRandomReturnValues(...randomValues: Array<number>) : void
{
	let randomIndex = 0;
	Math.random = () =>
	{
		return randomValues[randomIndex++];
	}
}