### 1.1.1
* Fixed an issue with the Range setting both min and max to the same value when providing one value to the constructor.

## 1.1.0
* Added readonly interfaces for the math objects.

### 1.0.2
* Fixed webpack not packing the module as a library.

### 1.0.1
* Excluded the unit test d.ts files from the release package.

# 1.0.0
* Initial Maths library.